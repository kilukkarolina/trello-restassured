package utils;

import base.BaseTest;

import static base.BaseTest.reqSpec;
import static io.restassured.RestAssured.given;

public class Utils {

    public static void deleteResource(String endpoint, String resourceId){

        given()
                .spec(reqSpec)
                .when()
                .delete(endpoint + "/" + resourceId)
                .then()
                .statusCode(200);
    }
}
