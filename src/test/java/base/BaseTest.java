package base;

import io.restassured.builder.RequestSpecBuilder;
import io.restassured.http.ContentType;
import io.restassured.specification.RequestSpecification;
import org.junit.jupiter.api.BeforeAll;

public class BaseTest {

    public static final String BASE_URL = "https://api.trello.com/1";
    public static final String ORGANIZATIONS = "organizations";
    public static final String BOARDS = "boards";
    public static final String LISTS = "lists";
    public static final String CARDS = "cards";

    protected static final String KEY = "YOUR_KEY";
    protected static final String TOKEN = "YOUR_TOKEN";

    public static RequestSpecBuilder reqBuilder;
    public static RequestSpecification reqSpec;

    @BeforeAll
    public static void beforeAll(){
        reqBuilder = new RequestSpecBuilder();
        reqBuilder.addQueryParam("key", KEY);
        reqBuilder.addQueryParam("token", TOKEN);
        reqBuilder.setContentType(ContentType.JSON);

        reqSpec = reqBuilder.build();
    }
}
