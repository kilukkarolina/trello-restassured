package organization;

import base.BaseTest;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import org.junit.jupiter.api.Test;
import utils.Utils;


import static io.restassured.RestAssured.given;
import static org.assertj.core.api.Assertions.*;


public class OrganizationTest extends BaseTest {

    private static String organizationId;

    @Test
    public void createNewOrganization(){

        Response response = given()
                .spec(reqSpec)
                .queryParam("displayName", "My first organization")
                .queryParam("desc", "First description")
                .queryParam("name", "my_first_organization")
                .queryParam("website", "http://firstorgtest.pl")
                .when()
                .post(BASE_URL + "/" + ORGANIZATIONS)
                .then()
                .statusCode(200)
                .extract()
                .response();

        JsonPath json = response.jsonPath();
        assertThat(json.getString("displayName")).isEqualTo("My first organization");
        assertThat(json.getString("desc")).isEqualTo("First description");
        assertThat(json.getString("name")).isEqualTo("my_first_organization");
        assertThat(json.getString("website")).isEqualTo("http://firstorgtest.pl");

        organizationId = json.get("id");
        Utils.deleteResource(BASE_URL + "/" + ORGANIZATIONS, organizationId);
    }

    @Test
    public void createOrganizationWithoutDisplayName(){

        Response response = given()
                .spec(reqSpec)
                .when()
                .post(BASE_URL + "/" + ORGANIZATIONS)
                .then()
                .statusCode(400)
                .extract()
                .response();
    }

    @Test
    public void createOrganizationWithCorrectName(){

        Response response = given()
                .spec(reqSpec)
                .queryParam("displayName", "My first organization")
                .queryParam("name", "my_first_organization_1")
                .when()
                .post(BASE_URL + "/" + ORGANIZATIONS)
                .then()
                .statusCode(200)
                .extract()
                .response();

        JsonPath json = response.jsonPath();
        assertThat(json.getString("displayName")).isEqualTo("My first organization");
        assertThat(json.getString("name")).isEqualTo("my_first_organization_1");

        organizationId = json.get("id");
        Utils.deleteResource(BASE_URL + "/" + ORGANIZATIONS, organizationId);
    }

    @Test
    public void createOrganizationWithInvalidLengthOfName(){

        Response response = given()
                .spec(reqSpec)
                .queryParam("displayName", "My first organization")
                .queryParam("name", "my")
                .when()
                .post(BASE_URL + "/" + ORGANIZATIONS)
                .then()
                .statusCode(200)
                .extract()
                .response();

        JsonPath json = response.jsonPath();
        assertThat(json.getString("displayName")).isEqualTo("My first organization");

        String organizationName = json.getString("name");
        assertThat(organizationName).startsWith("my");
        assertThat(organizationName.length() > 2).isTrue();

        organizationId = json.get("id");
        Utils.deleteResource(BASE_URL + "/" + ORGANIZATIONS, organizationId);
    }

    @Test
    public void createOrganizationWithInvalidName(){

        Response response = given()
                .spec(reqSpec)
                .queryParam("displayName", "My first organization")
                .queryParam("name", "MY_first/[organization]_1")
                .when()
                .post(BASE_URL + "/" + ORGANIZATIONS)
                .then()
                .statusCode(200)
                .extract()
                .response();

        JsonPath json = response.jsonPath();
        assertThat(json.getString("displayName")).isEqualTo("My first organization");
        assertThat(json.getString("name")).startsWith("my_firstorganization_1");

        organizationId = json.get("id");
        Utils.deleteResource(BASE_URL + "/" + ORGANIZATIONS, organizationId);
    }

    @Test
    public void createOrganizationWithHttpsWebsite(){

        Response response = given()
                .spec(reqSpec)
                .queryParam("displayName", "My first organization")
                .queryParam("website", "https://firstorgtest.pl")
                .when()
                .post(BASE_URL + "/" + ORGANIZATIONS)
                .then()
                .statusCode(200)
                .extract()
                .response();

        JsonPath json = response.jsonPath();
        assertThat(json.getString("displayName")).isEqualTo("My first organization");
        assertThat(json.getString("website")).isEqualTo("https://firstorgtest.pl");

        organizationId = json.get("id");
        Utils.deleteResource(BASE_URL + "/" + ORGANIZATIONS, organizationId);
    }

    @Test
    public void createOrganizationWithInvalidWebsite(){

        Response response = given()
                .spec(reqSpec)
                .queryParam("displayName", "My first organization")
                .queryParam("website", "www.firstorgtest.pl")
                .when()
                .post(BASE_URL + "/" + ORGANIZATIONS)
                .then()
                .statusCode(200)
                .extract()
                .response();

        JsonPath json = response.jsonPath();
        assertThat(json.getString("displayName")).isEqualTo("My first organization");
        assertThat(json.getString("website")).isEqualTo("http://www.firstorgtest.pl");

        organizationId = json.get("id");
        Utils.deleteResource(BASE_URL + "/" + ORGANIZATIONS, organizationId);
    }

}
